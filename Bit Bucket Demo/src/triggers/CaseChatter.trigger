trigger CaseChatter on Case (after insert, after update) {
    Id[] caseIds = new List<Id>();
    Id[] accountIds= new List<Id>();
    for (Case newCase : Trigger.new) {
        caseIds.add(newCase.id);
        accountIds.add(newCase.accountId);
    }
    ChatterUtils.copyFollowers(accountIds, caseIds);
}