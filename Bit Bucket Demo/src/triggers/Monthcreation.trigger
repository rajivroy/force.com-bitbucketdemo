trigger Monthcreation on Fee_Schedule__c (after insert, after update) {
    
    List<Id> feeScheduleId = new List<Id>();
    List<Fee_Schedule__c> fee = new List<Fee_Schedule__c>();
    List<Integer> Cmonth = new List<Integer>();
    List<Integer> CYear = new List<Integer>();
    List<Integer> Bmonth = new List<Integer>();
    List<Integer> BYear = new List<Integer>();
    List<String> Bmonthstring = new List<String>();
    Map<Id,Integer> Bmonthold = new Map<Id,Integer>();
    List<Integer> Bmonth1 = new List<Integer>();
    List<Integer> Cmonth1 = new List<Integer>();

    for(Fee_Schedule__c  f : Trigger.new){
        feeScheduleId.add(f.Id);
        fee.add(f);
        date d = f.Start_date_for_this_schedule__c;
        integer m = d.month(); 
        integer y = d.year();
        Cmonth.add(m);
        CYear.add(y);
    }
    
    //Get all Billable records for each fee schedule
    List<Billable__c> bill =[Select Id,Period_end_date__c,Fee_Schedule__c, Name,Unique__c from Billable__c where Fee_Schedule__c =: feeScheduleId];      
    Map<Id,Integer> countcurrentMonth = new Map<Id,Integer>();
    integer m1=0, result,countMonth;       
   
    List<integer> monthlist = new List<Integer>();
    List<Billable__c > billableinsert = new List<Billable__c >();
    Set<Billable__c > billableinsertSet = new Set<Billable__c >();
    List<Billable__c > Listbillableinsert = new List<Billable__c >();
   // Map<Id,Integer > number1 = new Map<Id,Integer>();
    List<Integer > number1 = new List<Integer>();

    integer count,currentday,nextmonthcount,t=0;
List<string> s = new List<String>();
List<string> s1 = new List<String>();

    for(integer i=0; i<fee.size(); i++){
  
        //Generate missing month records 
        for(integer j=0; j<bill.size(); j++){
            if((fee[i].Id == bill[j].Fee_Schedule__c)){
                date d = bill[j].Period_end_date__c;
                integer m = d.month(); 
                system.debug('mmmmmm'+m);
                integer y = d.year(); 
                Bmonth.add(m); 
                BYear.add(y);
                s = doConvertMonthIndex(m);
                s1.addAll(s);
                //system.debug('mnth '+mnth );
                if(s1[j] != bill[j].unique__c)
                 system.debug('çouttt12'+s1[j]);

            }
        } 
    }           
    
    for(integer i=0; i<s1.size(); i++){
  

    system.debug('çouttt'+s1[i]+'gggg'+i);}
    
    public List<String> doConvertMonthIndex(integer dbl) {
        List<string> coutt = new List<String>();
        string retval;
        
        if (dbl == 1) { retval = 'True';}
        else if (dbl == 2)  { retval = 'True';}
        else if (dbl == 3)  { retval = 'True';}
        else if (dbl == 4) { retval = 'True';}
        else if (dbl == 5)  { retval = 'True';}
        else if (dbl == 6)  { retval = 'True';}
        else if (dbl == 7)  { retval = 'True';}
        else if (dbl == 8)  { retval = 'True';}
        else if (dbl == 9)  { retval = 'True';}
        else if (dbl == 10)  { retval = 'True';}
        else if (dbl == 11)  { retval = 'True';}
        else if (dbl == 12)  { retval = 'True';}
        coutt.add(retval);
        return coutt;
    }            
    for(integer i=0; i<fee.size(); i++){
           
        //Generate missing month records 
        for(integer j=0; j<bill.size(); j++){
           // Integer cc = number1.get(bill[j].Id);
           // system.debug('cccccc'+cc);
           
            if((Cyear[i] == BYear[j])&&(fee[i].Id == bill[j].Fee_Schedule__c)){
                system.debug('bill[j].unique__c '+bill[j].unique__c);
                system.debug('jjjjj'+j);
                monthlist.add(cmonth[i]);
                count = monthlist[i];
                nextmonthcount = 1;
                system.debug('bill[j]');

                currentday = Endmonth.Currentmonth(count,BYear[j]);
                Billable__c b= new Billable__c ();
                b.Fee_Schedule__c = bill[j].Fee_Schedule__c;
                
                if((count > 12)){
                    integer year = BYear[j]+1;
                    currentday = Endmonth.Currentmonth(nextmonthcount,year);
                    system.debug('currentday'+currentday+'BYear[j]+1'+year);
                    Date myDate = date.newinstance(BYear[j]+1,nextmonthcount, currentday);
                    b.Period_end_date__c = myDate;
                    b.Unique__c = 'true';
                    nextmonthcount++;
                 }
                 else {
                     Date myDate = date.newinstance(BYear[j],count, currentday);
                     b.Period_end_date__c = myDate;
                     b.Unique__c = 'true';
                     count++;
                 }       
                 system.debug('count'+count);

                 billableinsertset.add(b);
                 }  
                 
                   
                 
            
            }
            //Generate records for all months if there is no billable records present 
            if (bill.size()== 0){
                monthlist.add(cmonth[i]);
                count = monthlist[i];
                nextmonthcount = 1;

            for(integer k=1; k<=12; k++){
                Billable__c b= new Billable__c ();

                currentday = Endmonth.Currentmonth(count,CYear[i]);
                // Billable__c b= new Billable__c ();
                b.Fee_Schedule__c = Fee[i].Id;
                
                    if((count > 12)){
                        integer year = CYear[i]+1;
                        currentday = Endmonth.Currentmonth(nextmonthcount,year);
                        system.debug('currentday'+currentday+'CYear[i]+1'+year);
                        Date myDate = date.newinstance(CYear[i]+1,nextmonthcount, currentday);
                        b.Period_end_date__c = myDate;
                        b.unique__c = 'true';
                       // number1.put(b.Id,nextmonthcount);
                        system.debug('bbbb123'+number1);

                        nextmonthcount++;
                    }
                    else {
                        Date myDate = date.newinstance(CYear[i],count, currentday);
                        b.Period_end_date__c = myDate;
                        b.Unique__c = 'true';
                       // number1.put(b.Id,count);
                        system.debug('bbbb'+number1);

                        count++;
                    } 
                    billableinsertset.add(b);
            } 
                     
            } 
        }
        billableinsert.addAll(billableinsertset);
        system.debug('mmmcount'+m1);
        system.debug('result'+result);
        system.debug('billableinsert'+billableinsert);

        upsert billableinsert ;
        
  
}