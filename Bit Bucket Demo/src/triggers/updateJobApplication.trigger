trigger updateJobApplication on Job_Application__c (before insert , before update) {
List<Id>Ids = new list<Id>();
List<Id>cid = new list<Id>();

for(Job_Application__c j:Trigger.New){
        if (j.Candidate__c != null){
         Ids.add(j.candidate__c); // add particular field record
         cid.add(j.id);     //add record id
            
         }
      }
List<position__c> p = [Select id,name from position__c where id IN:Ids];
for(integer i=0 ; i<Ids.size();i++){
    p[i].candidate__c = cid[i];
    }
    upsert(p);
 }