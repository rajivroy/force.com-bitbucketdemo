trigger Monthcreation1 on Fee_Schedule__c (after insert, after update) {
    
    List<Id> feeScheduleIdList = new List<Id>();
    List<Fee_Schedule__c> feeScheduleObjectList = new List<Fee_Schedule__c>();
    List<Integer> FeeScheduleCmonths = new List<Integer>();
    List<Integer> FeeScheduleCyears  = new List<Integer>();
    List<Integer> Billablemonths = new List<Integer>();
    List<Integer> Billableyears = new List<Integer>();
  
    for(Fee_Schedule__c  f : Trigger.new){
        if(f.Start_date_for_this_schedule__c != null){
            feeScheduleIdList.add(f.Id);
            feeScheduleObjectList.add(f);
            date d = f.Start_date_for_this_schedule__c;
            integer m = d.month(); 
            integer y = d.year();
            FeeScheduleCmonths.add(m);
            FeeScheduleCyears.add(y);
        }    
    }
    
    //Get all Billable records for each fee schedule
    List<Billable__c> billables =[Select Id,Period_end_date__c,Fee_Schedule__c, Name,Unique__c from Billable__c where Fee_Schedule__c =: feeScheduleIdList  Order by Period_end_date__c ASC];      
    
    List<Billable__c> RelatedBillables = new List<Billable__c>();
   
    for(integer i=0; i<feeScheduleObjectList.size(); i++){  
        //Getting all present months and years from child Billable object
        for(integer j=0; j<billables.size(); j++){
            if((feeScheduleObjectList[i].Id == billables[j].Fee_Schedule__c)){
                date d = billables[j].Period_end_date__c;
                integer m = d.month(); 
                integer y = d.year(); 
                Billablemonths.add(m); 
                Billableyears.add(y);
                RelatedBillables.add(billables[j]);
                system.debug('Billableyears'+Billableyears[j]);
                system.debug('Billablemonths'+Billablemonths[j]);
            }
        } 
    }           
    system.debug('RelatedBillables '+RelatedBillables.size());
    
    // Static array which holds 12 elements
    List<integer> Totalmonths = new List<integer>{1,2,3,4,5,6,7,8,9,10,11,12};
    
    List<Integer> Missingmonths = new List<Integer>();
    List<Integer> PresentYears = new List<Integer>();
    List<Integer> MissingYears = new List<Integer>();
    Integer currentYear; 
    
    //search thru Totalmonths array to see if any values there do not appear in Billablemonths array
    for (integer j = 0; Totalmonths.size() > j; j++){ 
        Boolean found=false;
        
        for (integer i = 0; Billablemonths.size() > i; i++){
            if (Billablemonths[i]==Totalmonths[j]) {         
                PresentYears.add(Billableyears[i]);
                system.debug('yearcc'+PresentYears);
                currentYear = Billableyears[i];
                found=true;
                break;
            }  

        }     
   
        if (!found) {
    
            Missingmonths.add(Totalmonths[j]);
            MissingYears.add(currentYear);
            system.debug('árrrr'+Missingmonths);
            system.debug('Currentyear'+MissingYears);
        }
        //end if
  
    }//end for j
    
    //Update MissingYears array if it contains null value
    for(Integer i=0; i<MissingYears.size(); i++){
        if(MissingYears[i] == null & PresentYears.size()>0)
            MissingYears[i] = PresentYears[0];
            system.debug('MissingYears[i]'+MissingYears);
    }
    
    Set<Billable__c >billableinsertset = new Set<Billable__c >();
    List<Billable__c >billableinsert = new List<Billable__c >();
    integer count,nextmonthcount,currentday;
    
    //Insertion of missing months 
    if(RelatedBillables.size()>0){
        for(integer k=0; k<Missingmonths.size(); k++){

        // if (Billablemonths[i] !=Missingmonths[k] && tdate == Billablef[i].Period_end_date__c) 
                
            Billable__c b = new Billable__c ();
       
            b.Fee_Schedule__c = RelatedBillables[0].Fee_Schedule__c;
            if(MissingYears[k] != null){
                currentday = Endmonth.Currentmonth(Missingmonths[k],MissingYears[k]);
                Date myDate = date.newinstance(MissingYears[k],Missingmonths[k], currentday);
                system.debug('Missingmonths[k]'+Missingmonths[k]);
                b.Period_end_date__c = myDate;
                billableinsertset.add(b); 
                system.debug(' billableinsertset'+ billableinsertset);
            }
        }   
        billableinsert.addAll(billableinsertset);
        if(billableinsert.size()>0)
            upsert billableinsert; 
    }
    
     
    List<Integer> feeScheduleMonths = new List<Integer>(); 
    
    //Generate records for all months if there is no billable records present 
    if (RelatedBillables.size()== 0){
        for(integer i=0; i<feeScheduleObjectList.size(); i++){
            feeScheduleMonths.add(FeeScheduleCmonths[i]);
            count = feeScheduleMonths[i];
            nextmonthcount = 1;

            for(integer k=1; k<=12; k++){
                Billable__c b= new Billable__c ();
                currentday = Endmonth.Currentmonth(count,FeeScheduleCyears[i]);
                b.Fee_Schedule__c = feeScheduleObjectList[i].Id;
                if((count > 12)){
                    integer year = FeeScheduleCyears[i]+1;
                    currentday = Endmonth.Currentmonth(nextmonthcount,year);
                    system.debug('currentday'+currentday+'FeeScheduleCyears [i]+1'+year);
                    Date myDate = date.newinstance(FeeScheduleCyears [i]+1,nextmonthcount, currentday);
                    b.Period_end_date__c = myDate;
                    nextmonthcount++;
                }
                else {
                    Date myDate = date.newinstance(FeeScheduleCyears [i],count, currentday);
                    b.Period_end_date__c = myDate;
                    count++;
                } 
                billableinsertset.add(b);
            } 
      
        }               
        billableinsert.addAll(billableinsertset);
        system.debug('billableinsert'+billableinsert);
        if(billableinsert.size()>0)
            upsert billableinsert ;
    }               
       
}