trigger chatterpost on FeedItem (after insert) {

List<Id> feeditemIds = new List<Id>();
for(FeedItem f: Trigger.New){
    feeditemIds.add(f.parentId);
    system.debug('f.Id'+f.parentId);
}

List<opportunityFeed> oppChatterValue = [Select body,Id,parentId from opportunityFeed where parentId IN: feeditemids order by CreatedDate DESC limit 1];
List<Id> oppId = new List<Id>();

Map<Id,opportunity> oppMap = new Map<Id,opportunity>([Select Id from opportunity where Id IN: feeditemIds]);
List<opportunity> oppInsert = new List<opportunity>();
Set<opportunity> oppInsertSet = new Set<opportunity>();

for(integer i=0; i<oppChatterValue.size(); i++){
    opportunity oppObject = oppMap.get(oppChatterValue[i].ParentId);
    oppObject.NextStep = oppChatterValue[i].body;
    oppinsertSet.add(oppObject);    
}    
oppinsert.addAll(oppinsertSet);

if(oppinsert.size() > 0)
    upsert oppinsert;

}