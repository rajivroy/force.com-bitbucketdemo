@isTest 
private class helloWorldAccountTriggerTest {

    static testMethod void testHello(){
        Account a = [select id,hello__c from account where name = 'ICICI'];
        
        Test.startTest();
        upsert a;
        Test.stopTest();
        
        System.assertEquals('World',a.hello__c);
        
}
}