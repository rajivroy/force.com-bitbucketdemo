public class ControllerChecklist {
    
    public List<ObjectValue> selectedOpportunity1{get;set;}
    public List<Task> selectedlist = new List<Task>();
    public ControllerChecklist(){
        
        string objectId = ApexPages.currentpage().getParameters().get('id');
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Map<String, String> keyPrefixMap = new Map<String, String>{};
        //get the prefix of the objects in Ids
        Set<String> keyPrefixSet = gd.keySet();
        for(String sObj : keyPrefixSet){
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
            String tempName = r.getName();
            String tempPrefix = r.getKeyPrefix();
            //in this map, all the Objects with their prefixes will be stored
            keyPrefixMap.put(tempPrefix,tempName);
        }
        String tPrefix = objectId;
        tPrefix = tPrefix.subString(0,3);
        //get the type of your specified object
        String objectType = keyPrefixMap.get(tPrefix);
        System.Debug(objectType);
        List<Task> oppinv1 = [select id from Task];

        List<checklist__c> check = [Select Object_Category__c, Object_Name__c from checklist__c where Object_Name__c =: objectType];
        selectedOpportunity1 = new List<ObjectValue>();
        for(integer j=0;j<oppinv1.size();j++){
            for(checkList__c o : [Select Object_Category__c, Object_Name__c from checklist__c where Object_Name__c =: objectType]){
                selectedOpportunity1.add(new ObjectValue(o, oppinv1[j]));
            }    
        }

    }
    String countries ;
  
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('US','US'));
        options.add(new SelectOption('CANADA','Canada'));
        options.add(new SelectOption('MEXICO','Mexico'));

        return options;
    }
 
    public String getCountries() {
        return countries;
    }
    

    public void setCountries(String countries) {
        this.countries = countries;
        system.debug('countries'+countries);
    }
    
    public class ObjectValue{
       public Checklist__c opp {get; set;}
       public Boolean selected {get; set;}
       public Task ii{get;set;}
       public ObjectValue(Checklist__c o,Task iii) {
           opp = o;
           selected = false;
       if(iii == NULL)
            {
                ii = new Task();
            }
            else
            ii = iii;
     }
     }

}