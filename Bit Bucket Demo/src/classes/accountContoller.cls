public with sharing class accountContoller {
    
    // Use of StandardSetController. Displaying all values in a table using page block table.  
    private final Account acct;
    List<Id> conIds = new List<Id>();
    public accountContoller(ApexPages.StandardSetController controller){  
        this.acct = (Account)controller.getRecord();   
        
    }    
    
    public ApexPages.StandardSetController accountRecords{
        get{
            if(accountRecords == null){
            
                return new ApexPages.StandardSetController(Database.getQueryLocator([Select name,Id,Email__c,phone from Account]));
            } 
        return accountRecords;     
        }  
        private set;    
    }  
      
    public List<Account> getAccountPagination(){
        return (List<Account>) accountRecords.getRecords();
    }
    
    public void testm(){}
}