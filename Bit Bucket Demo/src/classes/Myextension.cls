public class Myextension{
    private final Job_Application__c JB;
    public Myextension(ApexPages.StandardController stdController){
    this.JB =(Job_Application__c)stdController.getRecord();
    }
    public Review__c[] getReviewDetails(){
    Review__c[]ReviewList;
    List<Job_Application__c> job = [Select id from Job_Application__c];
    for(integer i=0;i<job.size();i++)
    ReviewList = [Select id,Candidate__c,Job_Application__c from Review__c where id=:job[i].id];
     return ReviewList;
    }
   }