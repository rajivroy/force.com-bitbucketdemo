@isTest 
private class GlobalSearchTest{

    static testMethod void testGlobalSearch(){
    
    // Perform our data preparation.
    ContentVersion content = new ContentVersion();
    content.title = 'Mobile';
    content.contenturl = 'www.google.com';
    insert content;
    
    Profile p = [select id from profile where name='Chatter Free User'];
    User u = new User(alias = 'standt', email='chatteruser@testorg.com',
        emailencodingkey='UTF-8',languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,lastname='Mobile',
        timezonesidkey='America/Los_Angeles', username='chatteruser@testorg.com');
    
    insert u;
        
    Intranet_Article__c ina = new Intranet_Article__c();
    ina.Name = 'Mobile';
    insert ina;
    
    FeedItem fd = new FeedItem();
    fd.body = 'Mobile';
    fd.parentId = u.Id; 
    insert fd;
  
    Search gs = new Search();
    
    // Start the test, this changes governor limit context to
    // that of class rather than test.

    Test.startTest();
    gs.searchBox = 'Mobile';
    gs.getSearchGlobal();

    System.assertEquals(content.title,'Mobile');
    System.assertEquals(fd.body,'Mobile');
    system.assert(u.lastname.startsWithIgnoreCase('Mobile'));
    System.assertEquals(ina.Name,'Mobile');

    Test.stopTest();
    // Stop the test, this changes limit context back to test from class.

    }
}