public class Search {

    public string searchBox{get;set;}
    public string photo{get;set;}
    public integer peopleCount{get;set;}
    public integer chatterCount{get;set;}
    public integer documentCount{get;set;}
    public integer articlesCount{get;set;}
    
    //Global method to call each method
    public Pagereference getSearchGlobal(){
        getContentVersions();
        getPostFiles();
        getPeople();
        getIntraArticles();
        return null;
    }
    
    //Get CRM Content with Full-Text search 
    public List<ContentVersion> getContentVersions(){
        documentCount = 0;
        List<contentVersion> cv ;
        if(searchBox != null){
            cv = [Select  Title from ContentVersion where Title =: searchBox];
            documentCount = cv.size();
        }

        return cv;
    }
    
    //Get Chatter Post or files
    public List<string> getPostFiles(){
        
        List<string> postOrFilelist = new List<string>();
        if(searchBox != null && searchbox != ''){
            List<FeedItem> fd = [Select ContentFileName,Body,Title from FeedItem where IsDeleted = false];
            system.debug('***fd***'+fd);
            for(integer i=0; i<fd.size(); i++){
                
                //Get Chatter Post
                if(fd[i].body != null){    
                    if(fd[i].body.containsIgnoreCase(searchBox)){
                        postOrFilelist.add(fd[i].body);
                    }
                }  
                        
                //Get Chatter files 

                if(fd[i].ContentFileName != null && fd[i].ContentFileName != '' && fd[i].ContentFileName.containsIgnoreCase(searchbox)){
                    postOrFilelist.add(fd[i].ContentFileName);
                } 
 
            }

        }       
        
        chatterCount = postOrFilelist.size();
        return postOrFilelist;
    }
    
    //Get Chatter people
    Public List<User> getPeople(){
        
        List<User> us;
        List<User> userReturn = new List<User>();
        peopleCount = 0;
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        if(gd.containsKey('FeedItem')){              //Check if Chatter enabled or not in current org
            if(searchBox != null && searchBox != ''){
                us = [Select Name,FullPhotoUrl from User];
                
                for(integer i=0; i<us.size(); i++){
                    if(searchbox != null){
                        if(us[i].Name.startsWithIgnoreCase(searchbox)){
                            photo = us[i].FullPhotoUrl;
                            peopleCount++;
                            userReturn.add(us[i]);
                        }    
                    }        
                }    
            }        
        }     
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Chatter is disabled'));

        } 
        
        return userReturn; 
    }
    
    public List<Intranet_Article__c> getIntraArticles(){
        
        List<Intranet_Article__c> articles = new List<Intranet_Article__c>();
        articlesCount = 0;
        if(searchBox != null){
            List<Intranet_Article__c> ina = [Select Name from Intranet_Article__c where Name =: searchBox];
            for(integer i=0; i<ina.size(); i++){
                articlesCount++;
                articles.add(ina[i]);
            }    
        }
       
        return articles;

    }     
               
}