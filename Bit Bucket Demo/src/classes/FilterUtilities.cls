public class FilterUtilities {

    private static final String REGEX_FILTER_CRITERIA = '[([0-9]{1,2})*|(\\()*|(\\))*|((AND)|(and)|(OR)|(or))*|(\\s)*]{1,1000}';
    
    private static final String REGEX_NUMBER_ONLY = '[0-9]';

    /**
     * Validate Filter Criteria
     * Criteria - criteria string
     * numberOfCriteria - total number of criteria
     **/
    public static Boolean isValidCriteria(String criteria, Integer totalNoOfCriteria) {
        //Step 1. Check Numbers 
        //1.1. Number must not repeat
        //1.2. Number must be between 1 and numberofCriteria
        //1.3. All condition number must be in criteria filter
        Set<Integer> criteriaSet = new Set<Integer>();        
        
        Matcher numberMatcher = Pattern.compile(REGEX_NUMBER_ONLY).matcher(criteria);
        while (numberMatcher.find()) {
            Integer val = Integer.valueOf(numberMatcher.group());
            
            //number > existing criteria
            if(val > totalNoOfCriteria) return false;
                        
            //duplicate number check
            if(criteriaSet.contains(val)) return false;
            
            criteriaSet.add(val);
        }
        
        for(Integer i=1; i < totalNoOfCriteria+1; i++) {
            if(!criteriaSet.contains(i))
                return false; 
        }
        
        //Step 2 - check brackets        
        if(!validParenthesis(criteria)) return false;        
        
        //Step 3 - check pattern        
        Matcher conMatcher = Pattern.compile(REGEX_FILTER_CRITERIA).matcher(criteria);
        if(!conMatcher.matches()) return false; 
        
        return true;
    }
    
    /**
     * Valid parenthesis of the supplied boolean expression.
     *   
     */
    public static Boolean validParenthesis(String booleanExpression) {
        if((parameterCounter(booleanExpression,'\\(') - parameterCounter(booleanExpression,'\\)')) != 0) {
            return false;
        }
        
        Integer length = booleanExpression.length();
        Integer openParenthesis = 0;
        Integer closeParenthesis = 0;       
        
        for (Integer i = 0; i < length; i++) {
            String charAt = charAt(booleanExpression,i);
            if(charAt != null) {
                if(charAt.equals('(')) {                    
                    openParenthesis++;
                } else if(charAt.equals(')')) {
                    closeParenthesis++;
                    if (openParenthesis < closeParenthesis) {
                        return false;
                    }
                }
            }
        }
        
        if (openParenthesis > closeParenthesis) {
            return false;
        }
        
        return true;
    }
    
    public static String charAt(String str, Integer index) {
        if(str == null){
            return null;
        }
        if(str.length() <= 0){
            return str; 
        }
        if(index < 0 || index >= str.length()){
            return null;    
        }
        return str.substring(index, index+1);
    }
    
    /**
     * Return total number of character/regex 
     **/
    private static Integer parameterCounter(String input, String regex) {
        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(input); // get a matcher object

        Integer count = 0;

        while(m.find()) {
            count++; 
        }
        
        return count;
    }
}