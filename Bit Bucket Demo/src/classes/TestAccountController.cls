public class TestAccountController {

    ApexPages.standardController acc;
    
    public TestAccountController(ApexPages.StandardController controller) {
        acc = controller;
    }
    
    public Pagereference SaveNewMethod(){
        Sobject accountObject = acc.getRecord();
        system.debug('AccountObject'+accountObject);
        upsert accountObject;
        system.debug('AccountObject1'+accountObject);
        string s = '/' + ('' + accountObject.get('Id')).subString(0, 3) + '/e?';
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
        return new Pagereference(s);
    }    
    
    public List<SelectOption> getCountries(){
    List<SelectOption> options = new List<SelectOption>();
        
    Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption(f.getLabel(), f.getValue()));
    }       
    return options;
    }
    
    public List<SelectOption> getStates(){
    
    List<SelectOption> optionsState = new List<SelectOption>();
        
    Schema.DescribeFieldResult fieldResult = Account.state__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
    for( Schema.PicklistEntry f : ple){
        
        optionsState.add(new SelectOption(f.getLabel(), f.getValue()));
    }       
    return optionsState;
    }
   // List<State_Code__c> asda = new List<State_Code__c>();
    
    public void getStateCode23(){
    
        //Set<State_Code__c> stateCodeSet = new Set<State_Code__c>();
       // List<State_Code__c> StateCodeValues = [Select Id, Name,Country_Name__c from State_Code__c]; 

     /*   Map<String,String> CompareState = new Map<Sting,String>();
        for(integer i=0; i<stateCode.size(); i++){
            CompareState.put(stateCode[i].country__c,stateCode[i].Name);
        }    
        stateCodeSet.addAll(stateCode);
    }
    
 */
        }
}