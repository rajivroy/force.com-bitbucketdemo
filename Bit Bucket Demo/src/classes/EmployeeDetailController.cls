public with sharing class EmployeeDetailController {
	
	public List<Employee__c> emplyeeList {get;set;}
	public Employee__c emplObj {get;set;}
	
	
	public EmployeeDetailController(){
		getEmployeeList();
	}
	
	public PageReference viewEmployeeDetails(){
		String emplId = Apexpages.currentPage().getParameters().get('emplid');
		emplObj = [Select Id,Employee_Name__c,Name from Employee__c where Id =: emplId limit 1];
	
		return null;
	}
	
	public void viewData(){
		
	}
	
	public PageReference updateEmployeeDetails(){
		update emplObj;
		PageReference pageRef = Page.Employee_S1;
		pageRef.setRedirect(true);
		return pageRef;
	}
	
	public PageReference delEmployeeDetails(){
		String emplId = Apexpages.currentPage().getParameters().get('emplid');
		emplObj = [Select Id,Employee_Name__c,Name from Employee__c where Id =: emplId limit 1];
		
		delete emplObj;
		PageReference pageRef = Page.Employee_S1;
		pageRef.setRedirect(true);
		return pageRef;
	}
	
	public List<Employee__c> getEmployeeList(){
		emplyeeList = [Select Id,Employee_Name__c,Name from Employee__c order by Employee_Name__c asc];
		return emplyeeList;
	}
	
	public void example(){
		system.debug('Testing values');	
			
	}
	
	public void example1(){
		system.debug('Testing values');	
		system.debug('Testing1');
		system.debug('Testing2');
		system.debug('Testing3');
		System.debug('hi');
		System.debug('hi'); 
		System.debug('hi');
		System.debug('hi');
		
	}
	
	
}