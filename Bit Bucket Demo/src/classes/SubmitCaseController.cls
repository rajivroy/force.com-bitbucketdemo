public class SubmitCaseController{
public Case c{get; set;}
public String acctNum{get; set;}
public SubmitCaseController(){
c=new case();}
public PageReference submitCase(){
List<Account>accts=[Select Id From Account where AccountNumber=:acctNum];
if(accts.size()!=1){
ApexPages.Message msg=new ApexPages.Message(ApexPages.Severity.FATAL,'Invalid account number');
ApexPages.addMessage(msg);
return null;
}else{
try{
        c.AccountId=accts.get(0).Id;
        contact cnt=[Select Id FROM Contact WHERE AccountId=:c.AccountId AND Email=:c.SuppliedEmail Limit 1];
        if(cnt!=null)
        c.contactId=cnt.Id;    
        Database.DMLoptions dmlopts=new Database.DMLoptions();
        dmlopts.assignmentRuleHeader.useDefaultRule=true;
        c.setoptions(dmlopts);
        Insert c;
        return new PageReference('/Thanks');
        }catch(Exception e){
        Apexpages.addmessages(e);
        return null;
            }
        }
    }
}